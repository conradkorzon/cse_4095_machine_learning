###############################################
###############################################
##_______________Conrad Korzon_______________##
##_________CSE 4095 - Machine Learning_______##
##_______________ Homework 1 ________________##
###############################################
###############################################
import numpy as np
import matplotlib.pyplot as plt

def g_function(t,x):
    theta_trans = np.transpose(t)
    z = theta_trans.dot(x)
    z = -1 * z
    return (1/(1+np.exp(z)))

def gradient_ascent(t, alpha, gradient):
    return (t + np.multiply(alpha,gradient))

def log_likelihood(theta,x,y):
    s = 0
    for i in range(len(x)):
        A = y[i] * np.log(g_function(theta, x[i]))
        B = (1-y[i])*np.log(1-g_function(theta,x[i]))
        s += A+B
    return s

def gradient_l_theta(t, xData, yData):
    s = np.array([0, 0, 0])
    for i in range(len(xData)):
        A = xData[i]
        B = g_function(t,A)
        C = yData[i] - B
        D = np.multiply(A,C)
        s = np.add(s,D)
    return s

def TwoNorm(vec):
    sum_of_parts = 0
    for i in vec:
        sum_of_parts += (i**2)
    return (sum_of_parts**0.5)

def CalculateProbability(x,y,theta):
    A = g_function(theta,x)**y
    B = (1-g_function(theta,x))**(1-y)
    return A*B

def RunGradientAscent(alpha, tolerance, shouldPlot=False, AdditionalPoints=[]):
    X = np.loadtxt('q1x.dat')
    ydata = np.loadtxt('q1y.dat')
    New_X = np.zeros(shape=(len(X),3))
    for i in range(len(X)):
        New_X[i][2] = X[i][1]
        New_X[i][1] = X[i][0]
        New_X[i][0] = 1
    print("----------------------------")
    print("Running Gradient Ascent with alpha = ", alpha," and tolerance: ", tolerance)
    theta = np.array([0, 0, 0])
    steps = 0
    cont = True
    while cont:
        delta_l_theta = gradient_l_theta(theta, New_X, ydata)
        if TwoNorm(delta_l_theta)<tolerance:
            cont = False
            break
        else:
            theta = gradient_ascent(theta,alpha,delta_l_theta)
            steps += 1
    print("Steps: ", steps)
    print("Theta: ", theta)
    print("----------------------------")
    if(shouldPlot):
        Labeled0 = False
        Labeled1 = False
        for i in range(len(New_X)):
            if (ydata[i] < 1):
                if not Labeled0:
                    plt.plot(New_X[i][1],New_X[i][2],'ks',label='Y = 0')
                    Labeled0 = True
                else:
                    plt.plot(New_X[i][1],New_X[i][2],'ks')
            else:
                if not Labeled1:
                    plt.plot(New_X[i][1],New_X[i][2],'gp',label='Y = 1')
                    Labeled1 = True
                else:
                    plt.plot(New_X[i][1],New_X[i][2],'gp')
        x = np.linspace(-3,11,100)
        y = (-1*(theta[0]+theta[1]*x)) / theta[2]
        plt.plot(x, y, '-r', label='Decision Boundary')
        plt.legend(loc='upper right')
        plt.axis([0, 8, -5, 6])
        plt.grid()
        
        if len(AdditionalPoints) > 0:
            ColorStrings = ['mo','bd','yp','gx']
            colorIndex = 0
            for pair in AdditionalPoints:
                a = pair[0]
                b = pair[1]
                plt.plot(a,b,ColorStrings[colorIndex%len(ColorStrings)])
                colorIndex+=1
        plt.show()
    return theta

#Exercise 2 Question 3.c.i
CalculatedTheta = RunGradientAscent(0.01,10**-6,True)

#Exercise 2 Question 3.e
#Note: Must close pyplot graph window to view the point probabilities
probability0 = CalculateProbability(np.array([1, 2, 1]),0,CalculatedTheta)
probability1 = CalculateProbability(np.array([1, 2, 1]),1,CalculatedTheta)
print("Probability 0: ", probability0)
print("Probability 1: ", probability1)

#Exercise 2 Question 3.c.ii
#RunGradientAscent(0.1,10**-6)
#Exercise 2 Question 3.c.iii
#RunGradientAscent(0.001,10**-6)
